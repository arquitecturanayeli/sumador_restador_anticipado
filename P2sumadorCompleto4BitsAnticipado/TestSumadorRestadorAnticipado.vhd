LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY TestSumadorRestadorAnticipado IS
END TestSumadorRestadorAnticipado;
 
ARCHITECTURE behavior OF TestSumadorRestadorAnticipado IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT sumadorC4BitsAnticipado
    PORT(
         A : IN  std_logic_vector(3 downto 0);
         B : IN  std_logic_vector(3 downto 0);
         S : OUT  std_logic_vector(3 downto 0);
         selector : IN  std_logic;
         Co : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal A : std_logic_vector(3 downto 0) := (others => '0');
   signal B : std_logic_vector(3 downto 0) := (others => '0');
   signal selector : std_logic := '0';

 	--Outputs
   signal S : std_logic_vector(3 downto 0);
   signal Co : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   --constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: sumadorC4BitsAnticipado PORT MAP (
          A => A,
          B => B,
          S => S,
          selector => selector,
          Co => Co
        );

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      A<="0101";
		B<="0011";
		
		wait for 100 ns;
		
		A<="0011";
		B<="0011";
		
		wait for 100 ns;
		
		A<="0111";
		B<="0001";
		
      -- insert stimulus here 

      wait;
   end process;

END;
