library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity sumadorC4BitsAnticipado is
    generic(
		n:integer:=4);
		
	port(
		A,B: in std_logic_vector(n-1 downto 0);
		S: out std_logic_vector(n-1 downto 0);
		selector: in std_logic;
		Co: out std_logic);
		
end sumadorC4BitsAnticipado;

architecture Behavioral of sumadorC4BitsAnticipado is
	signal C: std_logic_vector(n downto 0);
	signal G,P,EB: std_logic_vector(n-1 downto 0);
	
begin
	C(0)<=selector;
	
	oper:	for j in 0 to n-1 generate
		EB(j)<=B(j) xor selector;
		G(j)<=A(j) and EB(j);
		P(j)<=A(j) xor EB(j);
	
		S(j)<=P(j) xor C(j);
		C(j+1)<=G(j) or (C(j) and P(j));
		end generate oper;
		
		Co<= C(n);
end Behavioral;
