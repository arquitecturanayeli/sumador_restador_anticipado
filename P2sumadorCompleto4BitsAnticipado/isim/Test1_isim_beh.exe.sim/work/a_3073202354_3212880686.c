/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/juancarlos/Documentos/escom/7moSemestre/arquitectura/1Parcial/Practicas/P2sumadorCompleto4BitsAnticipado/P2sumadorCompleto4BitsAnticipado/SumadorRestador4BAnticipado.vhd";
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_3488546069778340532_503743352(char *, unsigned char , unsigned char );
unsigned char ieee_p_2592010699_sub_3488768496604610246_503743352(char *, unsigned char , unsigned char );
unsigned char ieee_p_2592010699_sub_3488768497506413324_503743352(char *, unsigned char , unsigned char );


static void work_a_3073202354_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;

LAB0:    xsi_set_current_line(21, ng0);

LAB3:    t1 = (t0 + 2568U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 11064);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = t3;
    xsi_driver_first_trans_delta(t1, 4U, 1, 0LL);

LAB2:    t8 = (t0 + 10648);
    *((int *)t8) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_1(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    unsigned char t12;
    unsigned char t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;

LAB0:    xsi_set_current_line(24, ng0);

LAB3:    t1 = (t0 + 2248U);
    t2 = *((char **)t1);
    t1 = (t0 + 3784U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 2568U);
    t11 = *((char **)t10);
    t12 = *((unsigned char *)t11);
    t13 = ieee_p_2592010699_sub_3488768497506413324_503743352(IEEE_P_2592010699, t9, t12);
    t10 = (t0 + 11128);
    t14 = (t10 + 56U);
    t15 = *((char **)t14);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    *((unsigned char *)t17) = t13;
    xsi_driver_first_trans_delta(t10, 3U, 1, 0LL);

LAB2:    t18 = (t0 + 10664);
    *((int *)t18) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_2(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(25, ng0);

LAB3:    t1 = (t0 + 2088U);
    t2 = *((char **)t1);
    t1 = (t0 + 3784U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 3368U);
    t11 = *((char **)t10);
    t10 = (t0 + 3784U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 3);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = ieee_p_2592010699_sub_3488768496604610246_503743352(IEEE_P_2592010699, t9, t18);
    t20 = (t0 + 11192);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = t19;
    xsi_driver_first_trans_delta(t20, 3U, 1, 0LL);

LAB2:    t25 = (t0 + 10680);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_3(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(26, ng0);

LAB3:    t1 = (t0 + 2088U);
    t2 = *((char **)t1);
    t1 = (t0 + 3784U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 3368U);
    t11 = *((char **)t10);
    t10 = (t0 + 3784U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 3);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = ieee_p_2592010699_sub_3488768497506413324_503743352(IEEE_P_2592010699, t9, t18);
    t20 = (t0 + 11256);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = t19;
    xsi_driver_first_trans_delta(t20, 3U, 1, 0LL);

LAB2:    t25 = (t0 + 10696);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_4(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(28, ng0);

LAB3:    t1 = (t0 + 3208U);
    t2 = *((char **)t1);
    t1 = (t0 + 3784U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 2888U);
    t11 = *((char **)t10);
    t10 = (t0 + 3784U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 4);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = ieee_p_2592010699_sub_3488768497506413324_503743352(IEEE_P_2592010699, t9, t18);
    t20 = (t0 + 11320);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = t19;
    xsi_driver_first_trans_delta(t20, 3U, 1, 0LL);

LAB2:    t25 = (t0 + 10712);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_5(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    char *t19;
    char *t20;
    char *t21;
    int t22;
    int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned char t27;
    unsigned char t28;
    unsigned char t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    char *t35;

LAB0:    xsi_set_current_line(29, ng0);

LAB3:    t1 = (t0 + 3048U);
    t2 = *((char **)t1);
    t1 = (t0 + 3784U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 2888U);
    t11 = *((char **)t10);
    t10 = (t0 + 3784U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 4);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = (t0 + 3208U);
    t20 = *((char **)t19);
    t19 = (t0 + 3784U);
    t21 = *((char **)t19);
    t22 = *((int *)t21);
    t23 = (t22 - 3);
    t24 = (t23 * -1);
    t25 = (1U * t24);
    t26 = (0 + t25);
    t19 = (t20 + t26);
    t27 = *((unsigned char *)t19);
    t28 = ieee_p_2592010699_sub_3488768496604610246_503743352(IEEE_P_2592010699, t18, t27);
    t29 = ieee_p_2592010699_sub_3488546069778340532_503743352(IEEE_P_2592010699, t9, t28);
    t30 = (t0 + 11384);
    t31 = (t30 + 56U);
    t32 = *((char **)t31);
    t33 = (t32 + 56U);
    t34 = *((char **)t33);
    *((unsigned char *)t34) = t29;
    xsi_driver_first_trans_delta(t30, 3U, 1, 0LL);

LAB2:    t35 = (t0 + 10728);
    *((int *)t35) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_6(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    unsigned char t12;
    unsigned char t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;

LAB0:    xsi_set_current_line(24, ng0);

LAB3:    t1 = (t0 + 2248U);
    t2 = *((char **)t1);
    t1 = (t0 + 3904U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 2568U);
    t11 = *((char **)t10);
    t12 = *((unsigned char *)t11);
    t13 = ieee_p_2592010699_sub_3488768497506413324_503743352(IEEE_P_2592010699, t9, t12);
    t10 = (t0 + 11448);
    t14 = (t10 + 56U);
    t15 = *((char **)t14);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    *((unsigned char *)t17) = t13;
    xsi_driver_first_trans_delta(t10, 2U, 1, 0LL);

LAB2:    t18 = (t0 + 10744);
    *((int *)t18) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_7(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(25, ng0);

LAB3:    t1 = (t0 + 2088U);
    t2 = *((char **)t1);
    t1 = (t0 + 3904U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 3368U);
    t11 = *((char **)t10);
    t10 = (t0 + 3904U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 3);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = ieee_p_2592010699_sub_3488768496604610246_503743352(IEEE_P_2592010699, t9, t18);
    t20 = (t0 + 11512);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = t19;
    xsi_driver_first_trans_delta(t20, 2U, 1, 0LL);

LAB2:    t25 = (t0 + 10760);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_8(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(26, ng0);

LAB3:    t1 = (t0 + 2088U);
    t2 = *((char **)t1);
    t1 = (t0 + 3904U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 3368U);
    t11 = *((char **)t10);
    t10 = (t0 + 3904U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 3);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = ieee_p_2592010699_sub_3488768497506413324_503743352(IEEE_P_2592010699, t9, t18);
    t20 = (t0 + 11576);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = t19;
    xsi_driver_first_trans_delta(t20, 2U, 1, 0LL);

LAB2:    t25 = (t0 + 10776);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_9(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(28, ng0);

LAB3:    t1 = (t0 + 3208U);
    t2 = *((char **)t1);
    t1 = (t0 + 3904U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 2888U);
    t11 = *((char **)t10);
    t10 = (t0 + 3904U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 4);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = ieee_p_2592010699_sub_3488768497506413324_503743352(IEEE_P_2592010699, t9, t18);
    t20 = (t0 + 11640);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = t19;
    xsi_driver_first_trans_delta(t20, 2U, 1, 0LL);

LAB2:    t25 = (t0 + 10792);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_10(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    char *t19;
    char *t20;
    char *t21;
    int t22;
    int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned char t27;
    unsigned char t28;
    unsigned char t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    char *t35;

LAB0:    xsi_set_current_line(29, ng0);

LAB3:    t1 = (t0 + 3048U);
    t2 = *((char **)t1);
    t1 = (t0 + 3904U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 2888U);
    t11 = *((char **)t10);
    t10 = (t0 + 3904U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 4);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = (t0 + 3208U);
    t20 = *((char **)t19);
    t19 = (t0 + 3904U);
    t21 = *((char **)t19);
    t22 = *((int *)t21);
    t23 = (t22 - 3);
    t24 = (t23 * -1);
    t25 = (1U * t24);
    t26 = (0 + t25);
    t19 = (t20 + t26);
    t27 = *((unsigned char *)t19);
    t28 = ieee_p_2592010699_sub_3488768496604610246_503743352(IEEE_P_2592010699, t18, t27);
    t29 = ieee_p_2592010699_sub_3488546069778340532_503743352(IEEE_P_2592010699, t9, t28);
    t30 = (t0 + 11704);
    t31 = (t30 + 56U);
    t32 = *((char **)t31);
    t33 = (t32 + 56U);
    t34 = *((char **)t33);
    *((unsigned char *)t34) = t29;
    xsi_driver_first_trans_delta(t30, 2U, 1, 0LL);

LAB2:    t35 = (t0 + 10808);
    *((int *)t35) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_11(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    unsigned char t12;
    unsigned char t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;

LAB0:    xsi_set_current_line(24, ng0);

LAB3:    t1 = (t0 + 2248U);
    t2 = *((char **)t1);
    t1 = (t0 + 4024U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 2568U);
    t11 = *((char **)t10);
    t12 = *((unsigned char *)t11);
    t13 = ieee_p_2592010699_sub_3488768497506413324_503743352(IEEE_P_2592010699, t9, t12);
    t10 = (t0 + 11768);
    t14 = (t10 + 56U);
    t15 = *((char **)t14);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    *((unsigned char *)t17) = t13;
    xsi_driver_first_trans_delta(t10, 1U, 1, 0LL);

LAB2:    t18 = (t0 + 10824);
    *((int *)t18) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_12(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(25, ng0);

LAB3:    t1 = (t0 + 2088U);
    t2 = *((char **)t1);
    t1 = (t0 + 4024U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 3368U);
    t11 = *((char **)t10);
    t10 = (t0 + 4024U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 3);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = ieee_p_2592010699_sub_3488768496604610246_503743352(IEEE_P_2592010699, t9, t18);
    t20 = (t0 + 11832);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = t19;
    xsi_driver_first_trans_delta(t20, 1U, 1, 0LL);

LAB2:    t25 = (t0 + 10840);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_13(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(26, ng0);

LAB3:    t1 = (t0 + 2088U);
    t2 = *((char **)t1);
    t1 = (t0 + 4024U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 3368U);
    t11 = *((char **)t10);
    t10 = (t0 + 4024U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 3);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = ieee_p_2592010699_sub_3488768497506413324_503743352(IEEE_P_2592010699, t9, t18);
    t20 = (t0 + 11896);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = t19;
    xsi_driver_first_trans_delta(t20, 1U, 1, 0LL);

LAB2:    t25 = (t0 + 10856);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_14(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(28, ng0);

LAB3:    t1 = (t0 + 3208U);
    t2 = *((char **)t1);
    t1 = (t0 + 4024U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 2888U);
    t11 = *((char **)t10);
    t10 = (t0 + 4024U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 4);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = ieee_p_2592010699_sub_3488768497506413324_503743352(IEEE_P_2592010699, t9, t18);
    t20 = (t0 + 11960);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = t19;
    xsi_driver_first_trans_delta(t20, 1U, 1, 0LL);

LAB2:    t25 = (t0 + 10872);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_15(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    char *t19;
    char *t20;
    char *t21;
    int t22;
    int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned char t27;
    unsigned char t28;
    unsigned char t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    char *t35;

LAB0:    xsi_set_current_line(29, ng0);

LAB3:    t1 = (t0 + 3048U);
    t2 = *((char **)t1);
    t1 = (t0 + 4024U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 2888U);
    t11 = *((char **)t10);
    t10 = (t0 + 4024U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 4);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = (t0 + 3208U);
    t20 = *((char **)t19);
    t19 = (t0 + 4024U);
    t21 = *((char **)t19);
    t22 = *((int *)t21);
    t23 = (t22 - 3);
    t24 = (t23 * -1);
    t25 = (1U * t24);
    t26 = (0 + t25);
    t19 = (t20 + t26);
    t27 = *((unsigned char *)t19);
    t28 = ieee_p_2592010699_sub_3488768496604610246_503743352(IEEE_P_2592010699, t18, t27);
    t29 = ieee_p_2592010699_sub_3488546069778340532_503743352(IEEE_P_2592010699, t9, t28);
    t30 = (t0 + 12024);
    t31 = (t30 + 56U);
    t32 = *((char **)t31);
    t33 = (t32 + 56U);
    t34 = *((char **)t33);
    *((unsigned char *)t34) = t29;
    xsi_driver_first_trans_delta(t30, 1U, 1, 0LL);

LAB2:    t35 = (t0 + 10888);
    *((int *)t35) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_16(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    unsigned char t12;
    unsigned char t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;

LAB0:    xsi_set_current_line(24, ng0);

LAB3:    t1 = (t0 + 2248U);
    t2 = *((char **)t1);
    t1 = (t0 + 4144U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 2568U);
    t11 = *((char **)t10);
    t12 = *((unsigned char *)t11);
    t13 = ieee_p_2592010699_sub_3488768497506413324_503743352(IEEE_P_2592010699, t9, t12);
    t10 = (t0 + 12088);
    t14 = (t10 + 56U);
    t15 = *((char **)t14);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    *((unsigned char *)t17) = t13;
    xsi_driver_first_trans_delta(t10, 0U, 1, 0LL);

LAB2:    t18 = (t0 + 10904);
    *((int *)t18) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_17(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(25, ng0);

LAB3:    t1 = (t0 + 2088U);
    t2 = *((char **)t1);
    t1 = (t0 + 4144U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 3368U);
    t11 = *((char **)t10);
    t10 = (t0 + 4144U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 3);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = ieee_p_2592010699_sub_3488768496604610246_503743352(IEEE_P_2592010699, t9, t18);
    t20 = (t0 + 12152);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = t19;
    xsi_driver_first_trans_delta(t20, 0U, 1, 0LL);

LAB2:    t25 = (t0 + 10920);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_18(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(26, ng0);

LAB3:    t1 = (t0 + 2088U);
    t2 = *((char **)t1);
    t1 = (t0 + 4144U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 3368U);
    t11 = *((char **)t10);
    t10 = (t0 + 4144U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 3);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = ieee_p_2592010699_sub_3488768497506413324_503743352(IEEE_P_2592010699, t9, t18);
    t20 = (t0 + 12216);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = t19;
    xsi_driver_first_trans_delta(t20, 0U, 1, 0LL);

LAB2:    t25 = (t0 + 10936);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_19(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(28, ng0);

LAB3:    t1 = (t0 + 3208U);
    t2 = *((char **)t1);
    t1 = (t0 + 4144U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 2888U);
    t11 = *((char **)t10);
    t10 = (t0 + 4144U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 4);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = ieee_p_2592010699_sub_3488768497506413324_503743352(IEEE_P_2592010699, t9, t18);
    t20 = (t0 + 12280);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = t19;
    xsi_driver_first_trans_delta(t20, 0U, 1, 0LL);

LAB2:    t25 = (t0 + 10952);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_20(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    char *t19;
    char *t20;
    char *t21;
    int t22;
    int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned char t27;
    unsigned char t28;
    unsigned char t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    char *t35;

LAB0:    xsi_set_current_line(29, ng0);

LAB3:    t1 = (t0 + 3048U);
    t2 = *((char **)t1);
    t1 = (t0 + 4144U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 2888U);
    t11 = *((char **)t10);
    t10 = (t0 + 4144U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 4);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = (t0 + 3208U);
    t20 = *((char **)t19);
    t19 = (t0 + 4144U);
    t21 = *((char **)t19);
    t22 = *((int *)t21);
    t23 = (t22 - 3);
    t24 = (t23 * -1);
    t25 = (1U * t24);
    t26 = (0 + t25);
    t19 = (t20 + t26);
    t27 = *((unsigned char *)t19);
    t28 = ieee_p_2592010699_sub_3488768496604610246_503743352(IEEE_P_2592010699, t18, t27);
    t29 = ieee_p_2592010699_sub_3488546069778340532_503743352(IEEE_P_2592010699, t9, t28);
    t30 = (t0 + 12344);
    t31 = (t30 + 56U);
    t32 = *((char **)t31);
    t33 = (t32 + 56U);
    t34 = *((char **)t33);
    *((unsigned char *)t34) = t29;
    xsi_driver_first_trans_delta(t30, 0U, 1, 0LL);

LAB2:    t35 = (t0 + 10968);
    *((int *)t35) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_21(char *t0)
{
    char *t1;
    char *t2;
    int t3;
    unsigned int t4;
    unsigned int t5;
    unsigned int t6;
    unsigned char t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;

LAB0:    xsi_set_current_line(32, ng0);

LAB3:    t1 = (t0 + 2888U);
    t2 = *((char **)t1);
    t3 = (4 - 4);
    t4 = (t3 * -1);
    t5 = (1U * t4);
    t6 = (0 + t5);
    t1 = (t2 + t6);
    t7 = *((unsigned char *)t1);
    t8 = (t0 + 12408);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    *((unsigned char *)t12) = t7;
    xsi_driver_first_trans_fast_port(t8);

LAB2:    t13 = (t0 + 10984);
    *((int *)t13) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}


extern void work_a_3073202354_3212880686_init()
{
	static char *pe[] = {(void *)work_a_3073202354_3212880686_p_0,(void *)work_a_3073202354_3212880686_p_1,(void *)work_a_3073202354_3212880686_p_2,(void *)work_a_3073202354_3212880686_p_3,(void *)work_a_3073202354_3212880686_p_4,(void *)work_a_3073202354_3212880686_p_5,(void *)work_a_3073202354_3212880686_p_6,(void *)work_a_3073202354_3212880686_p_7,(void *)work_a_3073202354_3212880686_p_8,(void *)work_a_3073202354_3212880686_p_9,(void *)work_a_3073202354_3212880686_p_10,(void *)work_a_3073202354_3212880686_p_11,(void *)work_a_3073202354_3212880686_p_12,(void *)work_a_3073202354_3212880686_p_13,(void *)work_a_3073202354_3212880686_p_14,(void *)work_a_3073202354_3212880686_p_15,(void *)work_a_3073202354_3212880686_p_16,(void *)work_a_3073202354_3212880686_p_17,(void *)work_a_3073202354_3212880686_p_18,(void *)work_a_3073202354_3212880686_p_19,(void *)work_a_3073202354_3212880686_p_20,(void *)work_a_3073202354_3212880686_p_21};
	xsi_register_didat("work_a_3073202354_3212880686", "isim/Test1_isim_beh.exe.sim/work/a_3073202354_3212880686.didat");
	xsi_register_executes(pe);
}
