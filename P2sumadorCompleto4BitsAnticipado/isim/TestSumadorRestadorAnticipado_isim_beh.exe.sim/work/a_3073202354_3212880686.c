/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/juancarlos/Documentos/escom/7moSemestre/arquitectura/1Parcial/Practicas/P2sumadorCompleto4BitsAnticipado/P2sumadorCompleto4BitsAnticipado/sumadorC4BitsAnticipado.vhd";
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_3488546069778340532_503743352(char *, unsigned char , unsigned char );
unsigned char ieee_p_2592010699_sub_3488768496604610246_503743352(char *, unsigned char , unsigned char );
unsigned char ieee_p_2592010699_sub_3488768497506413324_503743352(char *, unsigned char , unsigned char );


static void work_a_3073202354_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    unsigned char t12;
    unsigned char t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;

LAB0:    xsi_set_current_line(24, ng0);

LAB3:    t1 = (t0 + 2248U);
    t2 = *((char **)t1);
    t1 = (t0 + 3784U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 2568U);
    t11 = *((char **)t10);
    t12 = *((unsigned char *)t11);
    t13 = ieee_p_2592010699_sub_3488768497506413324_503743352(IEEE_P_2592010699, t9, t12);
    t10 = (t0 + 10800);
    t14 = (t10 + 56U);
    t15 = *((char **)t14);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    *((unsigned char *)t17) = t13;
    xsi_driver_first_trans_delta(t10, 3U, 1, 0LL);

LAB2:    t18 = (t0 + 10400);
    *((int *)t18) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_1(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(25, ng0);

LAB3:    t1 = (t0 + 2088U);
    t2 = *((char **)t1);
    t1 = (t0 + 3784U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 3368U);
    t11 = *((char **)t10);
    t10 = (t0 + 3784U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 3);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = ieee_p_2592010699_sub_3488768496604610246_503743352(IEEE_P_2592010699, t9, t18);
    t20 = (t0 + 10864);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = t19;
    xsi_driver_first_trans_delta(t20, 3U, 1, 0LL);

LAB2:    t25 = (t0 + 10416);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_2(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(26, ng0);

LAB3:    t1 = (t0 + 2088U);
    t2 = *((char **)t1);
    t1 = (t0 + 3784U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 3368U);
    t11 = *((char **)t10);
    t10 = (t0 + 3784U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 3);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = ieee_p_2592010699_sub_3488768497506413324_503743352(IEEE_P_2592010699, t9, t18);
    t20 = (t0 + 10928);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = t19;
    xsi_driver_first_trans_delta(t20, 3U, 1, 0LL);

LAB2:    t25 = (t0 + 10432);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_3(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(28, ng0);

LAB3:    t1 = (t0 + 3208U);
    t2 = *((char **)t1);
    t1 = (t0 + 3784U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 2888U);
    t11 = *((char **)t10);
    t10 = (t0 + 3784U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 3);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = ieee_p_2592010699_sub_3488768497506413324_503743352(IEEE_P_2592010699, t9, t18);
    t20 = (t0 + 10992);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = t19;
    xsi_driver_first_trans_delta(t20, 3U, 1, 0LL);

LAB2:    t25 = (t0 + 10448);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_4(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    char *t19;
    char *t20;
    char *t21;
    int t22;
    int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned char t27;
    unsigned char t28;
    unsigned char t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    char *t35;

LAB0:    xsi_set_current_line(29, ng0);

LAB3:    t1 = (t0 + 3048U);
    t2 = *((char **)t1);
    t1 = (t0 + 3784U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 2888U);
    t11 = *((char **)t10);
    t10 = (t0 + 3784U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 3);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = (t0 + 3208U);
    t20 = *((char **)t19);
    t19 = (t0 + 3784U);
    t21 = *((char **)t19);
    t22 = *((int *)t21);
    t23 = (t22 - 3);
    t24 = (t23 * -1);
    t25 = (1U * t24);
    t26 = (0 + t25);
    t19 = (t20 + t26);
    t27 = *((unsigned char *)t19);
    t28 = ieee_p_2592010699_sub_3488768496604610246_503743352(IEEE_P_2592010699, t18, t27);
    t29 = ieee_p_2592010699_sub_3488546069778340532_503743352(IEEE_P_2592010699, t9, t28);
    t30 = (t0 + 11056);
    t31 = (t30 + 56U);
    t32 = *((char **)t31);
    t33 = (t32 + 56U);
    t34 = *((char **)t33);
    *((unsigned char *)t34) = t29;
    xsi_driver_first_trans_delta(t30, 3U, 1, 0LL);

LAB2:    t35 = (t0 + 10464);
    *((int *)t35) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_5(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    unsigned char t12;
    unsigned char t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;

LAB0:    xsi_set_current_line(24, ng0);

LAB3:    t1 = (t0 + 2248U);
    t2 = *((char **)t1);
    t1 = (t0 + 3904U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 2568U);
    t11 = *((char **)t10);
    t12 = *((unsigned char *)t11);
    t13 = ieee_p_2592010699_sub_3488768497506413324_503743352(IEEE_P_2592010699, t9, t12);
    t10 = (t0 + 11120);
    t14 = (t10 + 56U);
    t15 = *((char **)t14);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    *((unsigned char *)t17) = t13;
    xsi_driver_first_trans_delta(t10, 2U, 1, 0LL);

LAB2:    t18 = (t0 + 10480);
    *((int *)t18) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_6(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(25, ng0);

LAB3:    t1 = (t0 + 2088U);
    t2 = *((char **)t1);
    t1 = (t0 + 3904U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 3368U);
    t11 = *((char **)t10);
    t10 = (t0 + 3904U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 3);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = ieee_p_2592010699_sub_3488768496604610246_503743352(IEEE_P_2592010699, t9, t18);
    t20 = (t0 + 11184);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = t19;
    xsi_driver_first_trans_delta(t20, 2U, 1, 0LL);

LAB2:    t25 = (t0 + 10496);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_7(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(26, ng0);

LAB3:    t1 = (t0 + 2088U);
    t2 = *((char **)t1);
    t1 = (t0 + 3904U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 3368U);
    t11 = *((char **)t10);
    t10 = (t0 + 3904U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 3);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = ieee_p_2592010699_sub_3488768497506413324_503743352(IEEE_P_2592010699, t9, t18);
    t20 = (t0 + 11248);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = t19;
    xsi_driver_first_trans_delta(t20, 2U, 1, 0LL);

LAB2:    t25 = (t0 + 10512);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_8(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(28, ng0);

LAB3:    t1 = (t0 + 3208U);
    t2 = *((char **)t1);
    t1 = (t0 + 3904U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 2888U);
    t11 = *((char **)t10);
    t10 = (t0 + 3904U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 3);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = ieee_p_2592010699_sub_3488768497506413324_503743352(IEEE_P_2592010699, t9, t18);
    t20 = (t0 + 11312);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = t19;
    xsi_driver_first_trans_delta(t20, 2U, 1, 0LL);

LAB2:    t25 = (t0 + 10528);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_9(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    char *t19;
    char *t20;
    char *t21;
    int t22;
    int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned char t27;
    unsigned char t28;
    unsigned char t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    char *t35;

LAB0:    xsi_set_current_line(29, ng0);

LAB3:    t1 = (t0 + 3048U);
    t2 = *((char **)t1);
    t1 = (t0 + 3904U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 2888U);
    t11 = *((char **)t10);
    t10 = (t0 + 3904U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 3);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = (t0 + 3208U);
    t20 = *((char **)t19);
    t19 = (t0 + 3904U);
    t21 = *((char **)t19);
    t22 = *((int *)t21);
    t23 = (t22 - 3);
    t24 = (t23 * -1);
    t25 = (1U * t24);
    t26 = (0 + t25);
    t19 = (t20 + t26);
    t27 = *((unsigned char *)t19);
    t28 = ieee_p_2592010699_sub_3488768496604610246_503743352(IEEE_P_2592010699, t18, t27);
    t29 = ieee_p_2592010699_sub_3488546069778340532_503743352(IEEE_P_2592010699, t9, t28);
    t30 = (t0 + 11376);
    t31 = (t30 + 56U);
    t32 = *((char **)t31);
    t33 = (t32 + 56U);
    t34 = *((char **)t33);
    *((unsigned char *)t34) = t29;
    xsi_driver_first_trans_delta(t30, 2U, 1, 0LL);

LAB2:    t35 = (t0 + 10544);
    *((int *)t35) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_10(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    unsigned char t12;
    unsigned char t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;

LAB0:    xsi_set_current_line(24, ng0);

LAB3:    t1 = (t0 + 2248U);
    t2 = *((char **)t1);
    t1 = (t0 + 4024U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 2568U);
    t11 = *((char **)t10);
    t12 = *((unsigned char *)t11);
    t13 = ieee_p_2592010699_sub_3488768497506413324_503743352(IEEE_P_2592010699, t9, t12);
    t10 = (t0 + 11440);
    t14 = (t10 + 56U);
    t15 = *((char **)t14);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    *((unsigned char *)t17) = t13;
    xsi_driver_first_trans_delta(t10, 1U, 1, 0LL);

LAB2:    t18 = (t0 + 10560);
    *((int *)t18) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_11(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(25, ng0);

LAB3:    t1 = (t0 + 2088U);
    t2 = *((char **)t1);
    t1 = (t0 + 4024U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 3368U);
    t11 = *((char **)t10);
    t10 = (t0 + 4024U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 3);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = ieee_p_2592010699_sub_3488768496604610246_503743352(IEEE_P_2592010699, t9, t18);
    t20 = (t0 + 11504);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = t19;
    xsi_driver_first_trans_delta(t20, 1U, 1, 0LL);

LAB2:    t25 = (t0 + 10576);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_12(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(26, ng0);

LAB3:    t1 = (t0 + 2088U);
    t2 = *((char **)t1);
    t1 = (t0 + 4024U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 3368U);
    t11 = *((char **)t10);
    t10 = (t0 + 4024U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 3);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = ieee_p_2592010699_sub_3488768497506413324_503743352(IEEE_P_2592010699, t9, t18);
    t20 = (t0 + 11568);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = t19;
    xsi_driver_first_trans_delta(t20, 1U, 1, 0LL);

LAB2:    t25 = (t0 + 10592);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_13(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(28, ng0);

LAB3:    t1 = (t0 + 3208U);
    t2 = *((char **)t1);
    t1 = (t0 + 4024U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 2888U);
    t11 = *((char **)t10);
    t10 = (t0 + 4024U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 3);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = ieee_p_2592010699_sub_3488768497506413324_503743352(IEEE_P_2592010699, t9, t18);
    t20 = (t0 + 11632);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = t19;
    xsi_driver_first_trans_delta(t20, 1U, 1, 0LL);

LAB2:    t25 = (t0 + 10608);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_14(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    char *t19;
    char *t20;
    char *t21;
    int t22;
    int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned char t27;
    unsigned char t28;
    unsigned char t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    char *t35;

LAB0:    xsi_set_current_line(29, ng0);

LAB3:    t1 = (t0 + 3048U);
    t2 = *((char **)t1);
    t1 = (t0 + 4024U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 2888U);
    t11 = *((char **)t10);
    t10 = (t0 + 4024U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 3);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = (t0 + 3208U);
    t20 = *((char **)t19);
    t19 = (t0 + 4024U);
    t21 = *((char **)t19);
    t22 = *((int *)t21);
    t23 = (t22 - 3);
    t24 = (t23 * -1);
    t25 = (1U * t24);
    t26 = (0 + t25);
    t19 = (t20 + t26);
    t27 = *((unsigned char *)t19);
    t28 = ieee_p_2592010699_sub_3488768496604610246_503743352(IEEE_P_2592010699, t18, t27);
    t29 = ieee_p_2592010699_sub_3488546069778340532_503743352(IEEE_P_2592010699, t9, t28);
    t30 = (t0 + 11696);
    t31 = (t30 + 56U);
    t32 = *((char **)t31);
    t33 = (t32 + 56U);
    t34 = *((char **)t33);
    *((unsigned char *)t34) = t29;
    xsi_driver_first_trans_delta(t30, 1U, 1, 0LL);

LAB2:    t35 = (t0 + 10624);
    *((int *)t35) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_15(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    unsigned char t12;
    unsigned char t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;

LAB0:    xsi_set_current_line(24, ng0);

LAB3:    t1 = (t0 + 2248U);
    t2 = *((char **)t1);
    t1 = (t0 + 4144U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 2568U);
    t11 = *((char **)t10);
    t12 = *((unsigned char *)t11);
    t13 = ieee_p_2592010699_sub_3488768497506413324_503743352(IEEE_P_2592010699, t9, t12);
    t10 = (t0 + 11760);
    t14 = (t10 + 56U);
    t15 = *((char **)t14);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    *((unsigned char *)t17) = t13;
    xsi_driver_first_trans_delta(t10, 0U, 1, 0LL);

LAB2:    t18 = (t0 + 10640);
    *((int *)t18) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_16(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(25, ng0);

LAB3:    t1 = (t0 + 2088U);
    t2 = *((char **)t1);
    t1 = (t0 + 4144U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 3368U);
    t11 = *((char **)t10);
    t10 = (t0 + 4144U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 3);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = ieee_p_2592010699_sub_3488768496604610246_503743352(IEEE_P_2592010699, t9, t18);
    t20 = (t0 + 11824);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = t19;
    xsi_driver_first_trans_delta(t20, 0U, 1, 0LL);

LAB2:    t25 = (t0 + 10656);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_17(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(26, ng0);

LAB3:    t1 = (t0 + 2088U);
    t2 = *((char **)t1);
    t1 = (t0 + 4144U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 3368U);
    t11 = *((char **)t10);
    t10 = (t0 + 4144U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 3);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = ieee_p_2592010699_sub_3488768497506413324_503743352(IEEE_P_2592010699, t9, t18);
    t20 = (t0 + 11888);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = t19;
    xsi_driver_first_trans_delta(t20, 0U, 1, 0LL);

LAB2:    t25 = (t0 + 10672);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_18(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    unsigned char t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;

LAB0:    xsi_set_current_line(28, ng0);

LAB3:    t1 = (t0 + 3208U);
    t2 = *((char **)t1);
    t1 = (t0 + 4144U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 2888U);
    t11 = *((char **)t10);
    t10 = (t0 + 4144U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 3);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = ieee_p_2592010699_sub_3488768497506413324_503743352(IEEE_P_2592010699, t9, t18);
    t20 = (t0 + 11952);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    *((unsigned char *)t24) = t19;
    xsi_driver_first_trans_delta(t20, 0U, 1, 0LL);

LAB2:    t25 = (t0 + 10688);
    *((int *)t25) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_19(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    int t4;
    int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned char t18;
    char *t19;
    char *t20;
    char *t21;
    int t22;
    int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned char t27;
    unsigned char t28;
    unsigned char t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    char *t35;

LAB0:    xsi_set_current_line(29, ng0);

LAB3:    t1 = (t0 + 3048U);
    t2 = *((char **)t1);
    t1 = (t0 + 4144U);
    t3 = *((char **)t1);
    t4 = *((int *)t3);
    t5 = (t4 - 3);
    t6 = (t5 * -1);
    t7 = (1U * t6);
    t8 = (0 + t7);
    t1 = (t2 + t8);
    t9 = *((unsigned char *)t1);
    t10 = (t0 + 2888U);
    t11 = *((char **)t10);
    t10 = (t0 + 4144U);
    t12 = *((char **)t10);
    t13 = *((int *)t12);
    t14 = (t13 - 3);
    t15 = (t14 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t10 = (t11 + t17);
    t18 = *((unsigned char *)t10);
    t19 = (t0 + 3208U);
    t20 = *((char **)t19);
    t19 = (t0 + 4144U);
    t21 = *((char **)t19);
    t22 = *((int *)t21);
    t23 = (t22 - 3);
    t24 = (t23 * -1);
    t25 = (1U * t24);
    t26 = (0 + t25);
    t19 = (t20 + t26);
    t27 = *((unsigned char *)t19);
    t28 = ieee_p_2592010699_sub_3488768496604610246_503743352(IEEE_P_2592010699, t18, t27);
    t29 = ieee_p_2592010699_sub_3488546069778340532_503743352(IEEE_P_2592010699, t9, t28);
    t30 = (t0 + 12016);
    t31 = (t30 + 56U);
    t32 = *((char **)t31);
    t33 = (t32 + 56U);
    t34 = *((char **)t33);
    *((unsigned char *)t34) = t29;
    xsi_driver_first_trans_delta(t30, 0U, 1, 0LL);

LAB2:    t35 = (t0 + 10704);
    *((int *)t35) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3073202354_3212880686_p_20(char *t0)
{
    char *t1;
    char *t2;
    int t3;
    int t4;
    unsigned int t5;
    unsigned int t6;
    unsigned int t7;
    unsigned char t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;

LAB0:    xsi_set_current_line(32, ng0);

LAB3:    t1 = (t0 + 2888U);
    t2 = *((char **)t1);
    t3 = (4 - 1);
    t4 = (t3 - 3);
    t5 = (t4 * -1);
    t6 = (1U * t5);
    t7 = (0 + t6);
    t1 = (t2 + t7);
    t8 = *((unsigned char *)t1);
    t9 = (t0 + 12080);
    t10 = (t9 + 56U);
    t11 = *((char **)t10);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    *((unsigned char *)t13) = t8;
    xsi_driver_first_trans_fast_port(t9);

LAB2:    t14 = (t0 + 10720);
    *((int *)t14) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}


extern void work_a_3073202354_3212880686_init()
{
	static char *pe[] = {(void *)work_a_3073202354_3212880686_p_0,(void *)work_a_3073202354_3212880686_p_1,(void *)work_a_3073202354_3212880686_p_2,(void *)work_a_3073202354_3212880686_p_3,(void *)work_a_3073202354_3212880686_p_4,(void *)work_a_3073202354_3212880686_p_5,(void *)work_a_3073202354_3212880686_p_6,(void *)work_a_3073202354_3212880686_p_7,(void *)work_a_3073202354_3212880686_p_8,(void *)work_a_3073202354_3212880686_p_9,(void *)work_a_3073202354_3212880686_p_10,(void *)work_a_3073202354_3212880686_p_11,(void *)work_a_3073202354_3212880686_p_12,(void *)work_a_3073202354_3212880686_p_13,(void *)work_a_3073202354_3212880686_p_14,(void *)work_a_3073202354_3212880686_p_15,(void *)work_a_3073202354_3212880686_p_16,(void *)work_a_3073202354_3212880686_p_17,(void *)work_a_3073202354_3212880686_p_18,(void *)work_a_3073202354_3212880686_p_19,(void *)work_a_3073202354_3212880686_p_20};
	xsi_register_didat("work_a_3073202354_3212880686", "isim/TestSumadorRestadorAnticipado_isim_beh.exe.sim/work/a_3073202354_3212880686.didat");
	xsi_register_executes(pe);
}
